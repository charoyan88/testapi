<?php
namespace App\Services;

/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 9:21 PM
 */
use App\Client;
use App\Exceptions\ClientNotFoundException;

class ClientService
{
    /**
     * @param $data
     * @return mixed
     */
    public function createClient($data)
    {
        return Client::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws ClientNotFoundException
     */
    public function getAllClients()
    {
        $clients = Client::all();
        if (!$clients)
            throw new ClientNotFoundException('Clients not found', 404);
        return $clients;
    }

    /**
     * @param $id
     * @return mixed
     * @throws ClientNotFoundException
     */

    public function getClientById($id)
    {
        $client = Client::find($id);
        if (!$client)
            throw new ClientNotFoundException('Client not found', 404);
        return $client;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws ClientNotFoundException
     */
    public function updateClientById($id, $data)
    {
        $client = Client::find($id);
        if ($client) {
            throw new ClientNotFoundException('Client not found', 404);
        }
        return $client->update($data);

    }

    /**
     * @param $id
     * @return mixed
     * @throws ClientNotFoundException
     */
    public function deleteClientById($id)
    {
        $client = Client::find($id);
        if (!$client)
            throw new ClientNotFoundException('Client not found', 404);
        return $client->delete();

    }
}