<?php
/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 10:26 PM
 */

namespace App\Services;

use App\Client;
use App\Token;

class TokenService
{
    public function generateToken($data)
    {
        return sha1($data . time());
    }

    public function createToken($app_id)
    {
        $tokenData = Token::where('app_id', $app_id)->first();
        if ($tokenData->token) {
            return base64_encode($tokenData['token']);
        } else {
            $token = $this->generateToken($app_id);
            $tokenData->token = $token;
            $tokenData->save();
            return base64_encode($token);
        }
    }
}