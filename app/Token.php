<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'client_id', 'token'
    ];
}
