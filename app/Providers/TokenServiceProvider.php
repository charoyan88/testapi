<?php
/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 10:28 PM
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class TokenServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(TokenService::class);
    }
}