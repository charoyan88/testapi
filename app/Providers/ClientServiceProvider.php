<?php
/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 9:27 PM
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ClientService::class);
    }
}