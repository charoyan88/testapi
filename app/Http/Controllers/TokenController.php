<?php
/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 10:25 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Services\TokenService;

class TokenController extends Controller
{
    public function getToken($app_id, TokenService $tokenService)
    {
        try {

            $token = $tokenService->createToken($app_id);
            $response = ['token' => $token, 'success' => true, 'error' => false, 'message' => 'Token successfully created!'];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);

    }
}