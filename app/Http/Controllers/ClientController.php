<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @param ClientService $clientService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, ClientService $clientService)
    {
        try {
            $this->validate($request, [
                'name' => 'required:clients',
                'email' => 'required|unique:clients',
                'phone' => 'required:clients',
                'address' => 'required:clients'
            ]);

            $data = $request->all();

            $client = $clientService->createClient($data);
            $response = ['data' => $client, 'success' => true, 'error' => false, 'message' => 'Client successfully created!'];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);

    }

    /**
     * @param ClientService $clientService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClients(ClientService $clientService)
    {
        try {
            $clients = $clientService->getAllClients();
            $response = ['data' => $clients, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);
    }

    /**
     * @param $id
     * @param ClientService $clientService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClient($id, ClientService $clientService)
    {
        try {
            $client = $clientService->getClientById($id);
            $response = ['data' => $client, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);
    }

    /**
     * @param $id
     * @param Request $request
     * @param ClientService $clientService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request, ClientService $clientService)
    {
        try {
            $data = $request->all();
            $client = $clientService->updateClientById($id, $data);
            $response = ['data' => $client, 'success' => true, 'error' => false, 'message' => 'Client successfully updated!'];
            $status = 200;

        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);
    }

    /**
     * @param $id
     * @param ClientService $clientService
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, ClientService $clientService)
    {
        try {
            $client = $clientService->deleteClientById($id);
            $response = ['data' => $client, 'success' => true, 'error' => false, 'message' => 'Client successfully removed!'];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 404;
        }
        return response()->json($response, $status);

    }
}
