<?php
/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/26/2018
 * Time: 10:04 PM
 */

namespace App\Http\Middleware;

use App\Token;
use Closure;
use Illuminate\Support\Facades\Auth;

class CustomAuth
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$request->header('authorization')) {
            return response()->json(array('error' => 'Please set Authorization  header for example(Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJleGFtcGxlLm9yZyIsImF1ZCI6ImV4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDB9.UQUJV7KmNWPiwiVFAqr4Kx6O6yd69lfbtyWF8qa8iMN2dpZZ1t6xaF8HUmY46y9pZN76f5UMGA0p_CMqymRdYfNiKsiTd2V_3Qpt9LObaLg6rq18j3GLHfdr8nyBzO3v7gTpmNaU6Xy47aMDsbcs593Lx_lD3PnO41oEHgih7CsRKW1WcW1radnpEhdDO7-GpmGOF6xUnpAlQ9EHqpqnIlZPbVoJg92Iwozn-07uuWrkyKUpYN4IPpstd1ks3cKlJ6FH-2ROiC4N0MVLxp4lhUyKhLdwgDWYH4tjtdrEVK0a3_zVtK1ukvriEJqMkfYHnE6Bwv_pv_-lRNy_y7m-YQ)'));
        }

        if (strpos($request->header('authorization'), 'Bearer') === false) {
            return response()->json(array('error' => 'invalid token'));
        } else {
            $token = explode(' ', $request->header('authorization'));
            $validToken = Token::where('token', base64_decode($token[1]))->first();
            if (!$validToken) {
                return response()->json(array('error' => 'invalid token'));
            }
        }
        return $next($request);
    }
}