<?php
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: HelpComp
 * Date: 12/27/2018
 * Time: 1:54 AM
 */
class AppIdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('tokens')->insert([
                ['app_id' => '12', 'token' => '', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
                ['app_id' => '25', 'token' => '', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
                ['app_id' => '38', 'token' => '', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ]);
        }
    }
}