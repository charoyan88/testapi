<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Client;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Model::unguard();

        DB::table('clients')->delete();

        $clients = array(
            ['name' => 'Ryan Chenkie', 'email' => 'ryanchenkie@gmail.com', 'phone' => '2121212', 'address' => 'test1'],
            ['name' => 'Chris Sevilleja', 'email' => 'chris@scotch.io', 'phone' => '21221212', 'address' => 'test2'],
            ['name' => 'Holly Lloyd', 'email' => 'holly@scotch.io', 'phone' => '414141', 'address' => 'test3'],
            ['name' => 'Adnan Kukic', 'email' => 'adnan@scotch.io', 'phone' => '2125535531212', 'address' => 'test4'],
            );

            // Loop through each user above and create the record for them in the database
            foreach ($clients as $client) {
                Client::create($client);
            }

            Model::reguard();
        $this->call(AppIdSeeder::class);
        }
}
