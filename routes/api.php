<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('authenticate/{app_id}', 'TokenController@getToken');

Route::group(['middleware' => ['customauth']], function () {
    Route::post('clients', 'ClientController@create');
    Route::get('clients', 'ClientController@getClients');
    Route::get('client/{id}', 'ClientController@getClient');
    Route::put('client/{id}', 'ClientController@update');
    Route::delete('client/{id}', 'ClientController@delete');

});
